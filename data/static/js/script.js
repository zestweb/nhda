
$(".navbar-toggler").on("click", function(e) {
  $(this).toggleClass("active");
});
$(".zoeken").on("click", function(e) {
  e.preventDefault();
  $('.searchbox').toggle();
});

$(".nav-item").on("click", function(e) {
  
  $(this).toggleClass("active");
});
/*
$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
*/

var marquee = document.querySelector('.marquee');

var marqueeLength = marquee.clientWidth;

var marqueeTravelTime = Math.ceil( marqueeLength / 100 );

marquee.style.animation = `scrollLeft ${marqueeTravelTime}s linear infinite`;

marquee.addEventListener('mouseover', (e)=>{
  marquee.style['animation-play-state'] = 'paused';
})

marquee.addEventListener('mouseout', (e)=>{
  marquee.style['animation-play-state'] = 'running';
})